<?php

    require_once '../include/allfile.php';

    $pid = $_POST['id'];
    $pname = $_POST["pname"];
    $pic = $_POST["pic"];
    $pmatrik =$_POST["pmatrik"];
    $psihat  = $_POST["psihat"]; 
    $pstat = $_POST["pstat"]; 
    $pemail = $_POST["pemail"]; 
    $pphone = $_POST["pphone"]; 
    $paddr = $_POST["paddr"];   

    $kkolej = $_POST["kkolej"]; 
    $kblog = $_POST["kblog"]; 
    $kbilik = $_POST["kbilik"]; 
 

    $afak = $_POST["afak"];  
    $athn = $_POST["athn"]; 
    $asem = $_POST["asem"]; 
    $acour = $_POST["acour"]; 
    $agpa = $_POST["agpa"]; 
    $acgpa = $_POST["acgpa"]; 

    $paname = $_POST["paname"];
    $paic = $_POST["paic"];
    $pameni = $_POST["pameni"];
    $pacerai = $_POST["pacerai"];
    $pasihat = $_POST["pasihat"];
    $pakerj = $_POST["pakerj"];


    $bname = $_POST["bname"];
    $bic = $_POST["bic"];
    $bkerj = $_POST["bkerj"];
    $bmaji = $_POST["bmaji"];
    $bsihat = $_POST["bsihat"];
    $bstat = $_POST["bstat"];

    $iname = $_POST["iname"];
    $iic = $_POST["iic"];
    $ikerj = $_POST["ikerj"];
    $imaji = $_POST["imaji"];
    $isihat = $_POST["isihat"];
    $istat = $_POST["istat"];

    $adbla = $_POST["adbla"];
    $adkerj = $_POST["adkerj"];

    $tname = $_POST["tname"];
    $tstatus = $_POST["tstatus"];
    $tjenis = $_POST["tjenis"]; 
    $tjum = $_POST["tjum"];


    //echo $tstatus;
    //echo $tname;
   // echo $tjenis;



   // echo $pname;
   // echo $pic;
    //echo $pmatrik;



    $stmt = $pdosql->prepare('SELECT * FROM prof WHERE p_id=?');
    $stmt->bindParam(1, $_POST['id'], PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if(!$row)
    {
        //die('nothing found');
        $stmt = $pdosql ->prepare ("INSERT INTO prof(
            p_id,
            p_name,
            p_ic,
            p_matrik,
            p_sihat,
            p_stat,
            p_email,
            p_phone,
            p_addr,
            k_kolej,
            k_blog,
            k_bilik,
            a_fak,
            a_thn,
            a_sem,
            a_cour,
            a_gpa,
            a_cgpa,
            pa_name,
            pa_ic,
            pa_meni,
            pa_cerai,
            pa_sihat,
            pa_kerja,
            b_name,
            b_ic,
            b_kerj,
            b_maji,
            b_sihat,
            b_stat,
            i_name,
            i_ic,
            i_kerj,
            i_maji,
            i_sihat,
            i_stat,
            ad_bla,
            ad_kerj,
            t_name,
            t_jenis,
            t_status,
            t_jum ) 
            VALUES (
                :pid,
                :pname,
                :upic,
                :pmatrik,
                :psihat,
                :pstat,
                :pemail,
                :pphone,
                :paddr,
                :kkolej,
                :kblog,
                :kbilik,
                :afak,
                :athn,
                :asem,
                :acour,
                :agpa,
                :acgpa,
                :paname,
                :paic,
                :pameni,
                :pacerai,
                :pasihat,
                :pakerj,
                :bname,
                :bic,
                :bkerj,
                :bmaji,
                :bsihat,
                :bstat,
                :iname,
                :iic,
                :ikerj,
                :imaji,
                :isihat,
                :istat,
                :adbla,
                :adkerj,
                :tname,
                :tjenis,
                :tstatus,
                :tjum)");
        
        
        $stmt -> bindParam (':pid',$pid);
        $stmt -> bindParam (':pname',$pname);
        $stmt -> bindParam (':upic',$pic);
        $stmt -> bindParam (':pmatrik',$pmatrik);
        $stmt -> bindParam (':psihat',$psihat);
        $stmt -> bindParam (':pstat',$pstat); 
        $stmt -> bindParam (':pemail',$pemail);
        $stmt -> bindParam (':pphone',$pphone);
        $stmt -> bindParam (':paddr',$paddr);
        $stmt -> bindParam (':kkolej',$kkolej);
        $stmt -> bindParam (':kblog',$kblog);
        $stmt -> bindParam (':kbilik',$kbilik);
        $stmt -> bindParam (':afak',$afak);
        $stmt -> bindParam (':athn',$athn);
        $stmt -> bindParam (':asem',$asem);
        $stmt -> bindParam (':acour',$acour);
        $stmt -> bindParam (':agpa',$agpa);
        $stmt -> bindParam (':acgpa',$acgpa);
        $stmt -> bindParam (':paname',$paname);
        $stmt -> bindParam (':paic',$paic);
        $stmt -> bindParam (':pameni',$pameni);
        $stmt -> bindParam (':pacerai',$pacerai);
        $stmt -> bindParam (':pasihat',$pasihat);
        $stmt -> bindParam (':pakerj',$pakerj);
        $stmt -> bindParam (':bname',$bname);
        $stmt -> bindParam (':bic',$bic);
        $stmt -> bindParam (':bkerj',$bkerj);
        $stmt -> bindParam (':bmaji',$bmaji);
        $stmt -> bindParam (':bsihat',$bsihat);
        $stmt -> bindParam (':bstat',$bstat); 
        $stmt -> bindParam (':iname',$iname);
        $stmt -> bindParam (':iic',$iic);
        $stmt -> bindParam (':ikerj',$ikerj);
        $stmt -> bindParam (':imaji',$imaji);
        $stmt -> bindParam (':isihat',$isihat);
        $stmt -> bindParam (':istat',$istat); 
        $stmt -> bindParam (':adbla',$adbla);
        $stmt -> bindParam (':adkerj',$adkerj);                                                          
        $stmt -> bindParam (':tname',$tname);
        $stmt -> bindParam (':tjenis',$tjenis);
        $stmt -> bindParam (':tstatus',$tstatus);
        $stmt -> bindParam (':tjum',$tjum);
        $stmt -> execute();
    }
    else
    {
        $stmt = $pdosql ->prepare ("UPDATE prof SET 
        p_name = '{$pname}' ,
        p_ic = '{$pic}' ,
        p_matrik = '{$pmatrik}',
        p_sihat = '{$psihat}',
        p_stat = '{$pstat}',
        p_email = '{$pemail}',
        p_phone = '{$pphone}',
        p_addr = '{$paddr}',
        k_kolej = '{$kkolej}',
        k_blog = '{$kblog}',
        k_bilik = '{$kbilik}',
        a_fak = '{$afak}',
        a_thn = '{$athn}',
        a_sem = '{$asem}',
        a_cour = '{$acour}',
        a_gpa = '{$agpa}',
        a_cgpa = '{$acgpa}',
        pa_name = '{$paname}',
        pa_ic = '{$paic}',
        pa_meni = '{$pameni}',
        pa_cerai = '{$pacerai}',
        pa_sihat = '{$pasihat}',
        pa_kerj = '{$pakerj}',
        b_name = '{$bname}',
        b_ic = '{$bic}',
        b_kerj = '{$bkerj}',
        b_maji = '{$bmaji}',
        b_sihat = '{$bsihat}',
        b_stat = '{$bstat}',
        i_name = '{$iname}',
        i_ic = '{$iic}',
        i_kerj = '{$ikerj}',
        i_maji = '{$imaji}',
        i_sihat = '{$isihat}',
        i_stat = '{$istat}',
        ad_bla = '{$adbla}',
        ad_kerj = '{$adkerj}',
        t_name= '{$tname}',
        t_jenis = '{$tjenis}',
        t_status = '{$tstatus}',
        t_jum = '{$tjum}'
        WHERE  p_id = '{$pid}' ");
        
        
        $stmt -> execute();

    }






?>